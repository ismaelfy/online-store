<?php

/**
 * summary
 */
class Notificacion
{
    private $datos;
    private $table = 'notificacion';
    private $cn;
    public function __construct()
    {
        $this->datos = array();
        require_once 'Conexion.php';
        $conect   = new Conexion();
        $this->cn = $conect->Conectar();
    }
    public function all()
    {
        $consulta = "SELECT * FROM $this->table where estado=0 ORDER BY id DESC";
        $resultado = $this->cn->query($consulta);
        if (mysqli_num_rows($resultado)) {
            while ($row = mysqli_fetch_object($resultado)) {
                $this->datos[] = $row;
            }
            return $this->datos;
        }
        return null;
    }

    public function save($data = null)
    {
        if ($data == null) {
            return false;
        }
        $keys    = [];
        $columns = [];
        foreach ($data as $key => $value) {
            $keys[]    = $key;
            $columns[] = "'{$value}'";
        }
        $columns = implode(', ', $columns);
        $keys    = implode(', ', $keys);

        $query = "INSERT INTO $this->table ({$keys}) VALUES ({$columns})";

        $result = $this->cn->query($query);

        if ($result) {
            return true;
        }
        return false;
    }
    public function update($data = null, $id = null)
    {
        if ($data == null || $id == null) {
            return false;
        }
        $datos = [];
        foreach ($data as $key => $value) {
            $datos[] = "$key='{$value}'";
        }
        $datos  = implode(', ', $datos);
        $query  = "UPDATE $this->table  SET $datos  where id={$id}";
        $result = $this->cn->query($query);
        if ($result) {
            return $this->cn->insert_id;
        }
        return false;
    }
}
