<?php
session_start();
require_once 'includes/functions.php';
require 'paypal/config.php';
require_once './paypal/paypal.php';
require_once './app/ventas.php';


if (!isset($_SESSION['payment_detail']) || empty($_SESSION['payment_detail'])) {
    header('Location: ./');
    die();
}

$payment  = $_SESSION['payment_detail'];


$ventas = new Ventas();
$apiContext = getApiContext();

$Process = new Paypal($apiContext);

if ((isset($_GET['success']) && $_GET['success'] == true) && ($payment->success && $payment->type == 3)) {
    $data = (object)$_GET;
    if ($payment->type == 1 || $payment->type == 2) {
        $update = [
            "confirm" => 0,
        ];
        $res = $ventas->update($update, $payment->id_venta);
    }
    if ($payment->type == 3) {
        $errors = 0;
        $errors = ($payment->success) ?  +0 : +1;
        $errors = ((isset($payment->paymentId) && isset($data->paymentId)) && ($payment->paymentId == $data->paymentId)) ? +0 : +1;
        $errors = ((isset($payment->paymentId) && isset($data->paymentId)) && ($payment->paymentId == '' && $data->paymentId == '')) ? +0 : +1;
        $errors = (isset($data->PayerID) && $data->PayerID != '') ? +0 : +1;
        $errors = (isset($data->token) && $data->token != '') ? +0 : +1;
        if ($errors == 0) {
            $payData = (object)["paymentId" => $payment->paymentId, "PayerID" => $data->PayerID];
            $pay = $Process->execute_payment($payData);
            if ($pay->state == 'approved') {
                $datos = [
                    "invoice_number" => $payment->invoice_number,
                    "payment_id" => $payment->paymentId,
                    "payer_id" => $data->PayerID,
                    "payment_token" => $data->token,
                    "confirm" => 1,
                ];
                $ventas->update($datos, $payment->id_venta);
            } else {
                header('Location: ./error?success=false');
                die();
            }
        }
    }
}
unset($_SESSION['payment_detail']);

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Gracias - Chiry </title>
    <?php display_link(); ?>
</head>

<body>
    <?php
    display_header();
    detail_cart();
    ?>
    <!-- visualizar carrito -->
    <section class="p-0 h-50">
        <div class="container py-5">
            <div class="row pt-3">
                <div class="col-sm-12">
                    <h3> Su compra se realizó exitosamente.</h3>
                </div>
            </div>
        </div>
    </section>

    <?php

    display_footer();
    display_script();
    ?>
    <script>
        jQuery(document).ready(function($) {
            setTimeout(function () {
            window.location.href = '<?=BASE_URL?>';
          }, 1500);
        });
        window.onload = function() {
            var loading = document.getElementById('loading');
            loading.style.visibility = 'hidden';
            loading.style.opacity = '0';
            loading.style.display = 'none';            
        }
        
    </script>

</body>

</html>