jQuery(document).ready(function($) { 	
 	$('.menu li:has(ul)').click(function(e) {
 		e.preventDefault();
 		if ($(this).hasClass('active')) {
 			$(this).removeClass('active');
 			$(this).children('ul').slideUp(400);
 		} else {
 			$('.menu li ul').slideUp(400);
 			$('.menu li').removeClass('active');
 			/*$(this).addClass('active');*/
 			$(this).toggleClass('active');
 			$(this).children('ul').slideDown(400);
 		}

 	});

 	$('.bt-menu').click(function() {
		 //$('.contenedor-menu .menu').slideToggle();
		 $(".nav-menu").toggleClass('active');
 	});

 	$(window).resize(function(){
 		if ($(document).width() >= 901 ) {
			$(".header").removeClass("active");
			$("body").removeClass("mobile");
 			//$('.contenedor-menu .menu').css('display', 'block');	
 		}
 		if ($(document).width() <= 900 ) {			 
			$(".header").addClass("active");
      		$("body").addClass("mobile");
			//$('.contenedor-menu .menu').css('display', 'none');	
 			$('.menu li ul').slideUp();
 			/* $('.menu li').removeClass('active'); */
 		}

 	});
 	/*obtener valor submenu*/
 	$('.menu li ul li a').click(function() {
 		window.location.href=$(this).attr('href');
 	});
	
	 if ($(document).width() <= 900 ) {
		$(".header").addClass("active");
		$("body").addClass("mobile");
	 }

	 $("body").delegate(".numero", "keypress", function(e) {
        var key = e.keyCode || e.which;
        var tecla = String.fromCharCode(key).toLowerCase();
        var numeros = "0123456789";
        if ($(this).hasClass('set')) {
            numeros = "0123456789 ";
        }
        if ($(this).hasClass('date')) {
            numeros = "0123456789/";
        }
        var especiales = [8, 37, 39, 46];
        var tecla_especial = false;
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = false;
                break;
            }
        }
        if (numeros.indexOf(tecla) == -1 && !tecla_especial) return false;
    });
 });

 