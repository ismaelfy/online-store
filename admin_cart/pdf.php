<?php
require_once './vendor/autoload.php';
require_once '../paypal/config.php';
require 'app/Pedidos.php';
if (isset($_GET['id']) && trim($_GET['id']) != '') {
    //$head = explode('/', $_GET['id']);
    if (trim($_GET['id']) != '') {
        $id = (int) filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);

        $pedidos  = new Pedidos();
        $doc      = $pedidos->find($id);
        $subtotal = 0;
        
        $pedidos = new Pedidos();
        $details = $pedidos->find_detail($id);
        foreach ($details as $value) {
            $subtotal += $value->importe;
        }

        $datos = [
            "doc"      => $doc->ndoc,
            "fecha"    => $doc->fecha,
            "cliente"  => $doc->cliente,
            "address"  => $doc->direccion,
            "importe"  => $doc->importe,
            "envio"    => $doc->envio,
            "total"    => $doc->total,
            "details"  => $details,
            "subtotal" => $subtotal,
        ];

        $loader = new \Twig\Loader\FilesystemLoader('./invoice');
        $twig   = new \Twig\Environment($loader, array('debug' => true));

        $content = $twig->render('doc.html.twig', ['datos' => $datos]);

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [210, 270], 'orientation' => 'L']);

        //$content = file_get_contents('invoice/doc.tiwg');

        $mpdf->WriteHTML($content);
        $mpdf->Output();
    } else {
        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [148, 148], 'orientation' => 'L']);
        $mpdf->WriteHTML("Ocurrió error al cargar pdf");
        $mpdf->Output();
    }
} else {
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [148, 148], 'orientation' => 'L']);
    $mpdf->WriteHTML("Ocurrió error al cargar pdf");
    $mpdf->Output();
}
