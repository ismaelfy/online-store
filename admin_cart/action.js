let content = $("#grid-body");

function get_state(state, value = '') {
    const {
        confirm,
        status
    } = state;
    if (value != '') {
        if (confirm != null) {
            return ` ${value} <span class=" text-${confirm[1]} py-1 px-2"> ${confirm[0]} </span>`;
        } else {
            return value;
        }
    } else {
        if (status != null) {
            return `<span class="badge badge-${status[1]} py-1 px-2"> ${status[0]} </span>`;
        } else {
            return '';
        }
    }
}

function create_element(data) {
    if (data != undefined || data.length > 0) {
        let element = '';
        $.each(data, function(index, item) {
            let action = `
                  <div class="dropdown text-center">
                    <button class="btn-round btn-primary " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item completed" data-id="${item.id}" href="#"> Completado </a>
                      <a class="dropdown-item cancelled" data-id="${item.id}" href="#"> Cancelado </a>                  
                    </div>
                  </div>
                    `;
            element += `
              <tr>
                <td class="details-control">
                  <i data-id="${item.id}" class="fas fa-angle-right"></i>
                </td>
                <td> ${item.ndoc} </td>
                <td> ${item.fecha} </td>
                <td> ${item.cliente} </td>
                <td> ${item.direccion} </td>
                <td> ${item.telefono} </td>
                <td> ${item.total} </td>
                <td>                
                    ${get_state(item.state,item.pago)}
                </td>
                <td> ${get_state(item.state)} </td>
                <td> <a href="#" class="pdf" data-id="${item.id}"><i class="far fa-2x fa-file-pdf"></i></a> </td>
                <td>
               ${(item.status==0?action : '')}
                </td>
              </tr>
            `;
        });
        return element;
    } else {
        return `<tr><td colspan="10" align="center"> No tienes pedidos registrados </td></tr>`;
    }
}

function load_Data() {
    $.ajax({
        url: "./validar",
        type: "POST",
        dataType: "json",
        Async: false,
        data: {
            pedidos: 1,
            action: 'get'
        },
        beforeSend: function() {
            content.html(`<tr align="center">
          <td align="center">
            <img class="loading-gif" width="40" src="../img/loading.gif"/>
          </td>
        </tr>`);
        },
    }).done(function(response) {
        const {
            data,
            status
        } = response;
        if (status) {
            content.html("");
            content.append(create_element(data));
            let table = $("#table").DataTable();
            $("#table tbody").on("click", "td.details-control", function() {
                var tr = $(this).parents("tr");
                var row = table.row(tr);
                if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass("shown");
                } else {
                    let element = detail_items(row.data());
                    row.child(element).show();
                    tr.addClass("shown");
                }
            });
        } else {
            content.html(`<tr><td colspan="10" align="center"> No tienes pedidos registrados </td></tr>`);
        }
    }).fail(function() {
        console.log("error");
    });
}

function stringToHTML(str, type = false) {
    const html = document.implementation.createHTMLDocument();
    if (!type) {
        html.body.innerHTML = str[0];
        let element = html.body.children[0];
        return element.dataset.id;
    } else {
        html.body.innerHTML = str;
        let element = html.body.children[0];
        return element;
    }
}

function detail_content(item) {
    return `<tr>
            <td> ${item.descripcion} </td>
            <td> ${item.cantidad} </td>
            <td> ${item.precio} </td>
            <td> ${item.importe} </td>
        </tr>`;
}

function detail_items(d) {
    let id = stringToHTML(d);
    let element = "";
    $.ajax({
        url: "./validar",
        type: "POST",
        async: false,
        dataType: "json",
        data: {
            detail: 1,
            id: id,
        },
    }).done(function(response) {
        const {
            data,
            status
        } = response;
        if (status) {
            $.each(data, function(index, item) {
                element += detail_content(item);
            });
        }
        if (!status) {
            element += `<tr> <td> Hubo error al cargar </td> </tr>`;
        }
    });
    let _row = `<table class="table table-sm">
        <thead>
          <th> Producto </th>
          <th> Cantidad </th>
          <th> Precio </th>
          <th> Importe </th>
        </thead>
        <tbody> `;
    _row += element;
    _row += `</tbody>
      </table>`;
    return stringToHTML(_row, true);
}

function update_status(type, id) {
    $.ajax({
        url: './validar.php',
        type: 'POST',
        dataType: 'json',
        data: {
            pedidos: 1,
            action: 'update',
            type: type,
            id: id
        },
    }).done(function(response) {
        return response;
    }).fail(function() {
        return null;
    })
}
jQuery(document).ready(function($) {
    Producto();
    if (content.length) {
        load_Data();
    }
    $('body').delegate('.completed', 'click', function(event) {
        event.preventDefault();
        const response = update_status(1, this.dataset.id);
        load_Data();
    });
    $('body').delegate('.cancelled', 'click', function(event) {
        event.preventDefault();
        const response = update_status(2, this.dataset.id);
        load_Data();
    });
    $("body").delegate(".addProd ", "click", function(event) {
        event.preventDefault();
        $.ajax({
            url: "validar.php",
            method: "POST",
            data: {
                newPro: 1,
            },
            beforeSend: function() {
                $(".contenedor-item").html('<img class="loading-gif" src="../img/loading.gif">');
            },
        }).done(function(data) {
            setTimeout(function() {
                $(".contenedor-item").html(data);
            }, 500);
        });
    });
    /*add new product menu*/
    $("body").delegate(".addproduct ", "click", function(event) {
        event.preventDefault();
        $.ajax({
            url: "validar.php",
            method: "POST",
            data: {
                newPro: 1,
            },
            beforeSend: function() {
                $(".contenedor-item").html('<img class="loading-gif" src="../img/loading.gif">');
            },
        }).done(function(data) {
            setTimeout(function() {
                $(".contenedor-item").html(data);
            }, 500);
        });
    });
    /*save product*/
    $("body").delegate("#new_product", "submit", function(e) {
        e.preventDefault();
        var form = new FormData($(this)[0]);
        $.ajax({
            url: "RegisterProducto.php",
            method: "POST",
            processData: false,
            contentType: false,
            data: form,
            beforeSend: function() {
                $(".contenedor-item").html('<img class="loading-gif" src="../img/loading.gif">');
            },
        }).done(function(data) {
            $(".sms").html(data);
            setTimeout(function() {
                Producto();
            }, 500);
        });
    });
    /*   select list  page  */
    $("body").delegate(".numpage ", "click", function(event) {
        event.preventDefault();
        var valor = $(this).attr("href");
        $.ajax({
            url: "validar.php",
            method: "POST",
            data: {
                product: 1,
                idnext: valor,
                action: 'get'
            },
        }).done(function(data) {
            setTimeout(function() {
                $(".contenedor-item").html(data);
            }, 300);
        });
    });
    $('body').delegate('.logout', 'click', function(event) {
        event.preventDefault();
        $.ajax({
            url: 'validar.php',
            type: 'POST',
            dataType: 'json',
            data: {
                logout: 1
            },
        }).done(function(response) {
            if (response.success) {
                swal({
                    title: "Success!",
                    text: response.msg,
                    icon: "success",
                    timer: 500
                });
                window.location.href = './';
            }
        }).fail(function() {
            console.log("error");
        });
    });
    $("#login_form").submit(function(event) {
        event.preventDefault();
        var usu = $(this).find("#user").val();
        var pwd = $(this).find("#pwd").val();
        $.ajax({
            url: "validar.php",
            method: "POST",
            data: {
                log_a: 1,
                user: usu,
                pass: pwd,
            },
        }).done(function(data) {
            $(".smslog").html(data);
        });
    });
    let idprod = 0;
    $('body').delegate('.stock', 'click', function(event) {
        event.preventDefault();
        $('#myModal').html(stock_template());
        $('#myModal').modal('show')
        idprod = this.dataset.id;
    });
    $('body').delegate('.btn-stock', 'click', function(event) {
        event.preventDefault();
        let value = $('#stock_value');
        let type = $("input:radio[name='stock_type']:checked").val();
        value.css('border', '1px solid #ced4da');
        if (value.val().trim() == '') {
            value.css('border', '1px solid red');
            return false;
        }
        $.ajax({
            url: 'validar.php',
            type: 'POST',
            dataType: 'json',
            data: {
                product: idprod,
                action: 'stock',
                type: type,
                cantidad: value.val().trim()
            },
        }).done(function(response) {
            if (response.success) {
                swal({
                    title: "success",
                    text: response.msg,
                    icon: "success",
                    timer: 1000
                });
                idprod = 0;
                $('#myModal').modal('hide')
                Producto();
            } else {
                swal({
                    title: "error",
                    text: response.msg,
                    icon: "error",
                    timer: 1000
                });
            }
        }).fail(function() {
            console.log("error");
        });
    });
    // notificacion
    $('body').delegate('.notif', 'click', function(event) {
        event.preventDefault();
        $.ajax({
            url: 'validar.php',
            type: 'POST',
            dataType: 'json',
            data: {
                notif: 1,
                type: 'update'
            },
        }).done(function(response) {
            if (response.success) {
                get_notif();
            }
        }).fail(function() {
            console.log("error");
        });
    });
    // pdf
    $('body').delegate('.pdf', 'click', function(event) {
        id = this.dataset.id;
        var altura = 400;
        var anchura = 800;
        var y = parseInt((window.screen.height / 2) - (altura / 2) - 10);
        var x = parseInt((window.screen.width / 2) - (anchura / 2));
        printTicket = window.open('./pdf?id='+id, target = 'blank', 'width=' + anchura + ',height=' + altura + ',top=' + y + ',left=' + x + ',toolbar=no,location=no,menubar=no,scrollbars=no,')
    });
    get_notif();
});

function layout_notif(data) {
    let notif = $('.notif-box');
    let element = ` 
        <a href="#" type="button" id="drop_notif" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
            <i class="fa fa-bell"></i> 
            <span> ${(data!=null?data.length:0)} </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="drop_notif">
            <ul class="list-group list-group-flush">
    `;
    if (data != null) {
        $.each(data, function(index, item) {
            element += `<li class="list-group-item"> pedido N°: ${item.descripcion} </li>`;
        });
    } else {
        element += `<li class="p-2 border-bottom"> No hay pedidos pendientes </li>`;
    }
    element += `
                <li class="d-flex justify-content-center">
                    <a href="#" class="notif"> Marcar como leidos </a>
                </li>
            </ul>
        </div>
    `;
    notif.html(element)
}

function get_notif() {
    $.ajax({
        url: 'validar.php',
        type: 'POST',
        dataType: 'json',
        data: {
            notif: 1,
            type: 'get'
        },
    }).done(function(response) {
        if (response.success) {
            layout_notif(response.data);
        }
    });
}

function stock_template() {
    return `
    <div class="modal-dialog sm" role="document">
    <div class="modal-content">
      <div class="modal-header p-2">
        <h5 class="modal-title text-center w-100"> Actualizar estock </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form id="form-stock" class="form-row d-flex justify-content-center">
          <div class="form-group col-6 mb-2">
            <div class="custom-control custom-radio text-right">
              <input type="radio" id="up" name="stock_type" checked class="custom-control-input" value="1"/>
              <label class="custom-control-label" for="up"> aumentar </label>
            </div>            
          </div>
          <div class="form-group col-6 mb-2">
            <div class="custom-control custom-radio text-left">
              <input type="radio" id="down" name="stock_type" class="custom-control-input" value="2"/>
              <label class="custom-control-label" for="down"> Disminuir</label>
            </div>
          </div>

          <div class="form-group col-6">
            <label for="stock_value" class="col-form-label"> Cantidad </label>
            <input type="text" class="form-control numero" required name="stock_value" id="stock_value"/>
          </div>
          <div class="form-group col-12 d-flex justify-content-center">
            <button type="button" class="btn btn-primary btn-stock"> Actualizar </button>
          </div>
        </form>
      </div>
      
    </div>
  </div>
  `;
}

function Producto() {
    $.ajax({
        url: "validar.php",
        method: "POST",
        data: {
            product: 1,
            action: 'get'
        },
        beforeSend: function() {
            $(".contenedor-item").html('<img class="loading-gif" src="../img/loading.gif">');
        },
    }).done(function(data) {
        setTimeout(function() {
            $(".contenedor-item").html(data);
        }, 300);
    });
}