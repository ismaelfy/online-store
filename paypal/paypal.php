<?php

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;

class Paypal
{
    private $apiContext;
    public function __construct($context = null)
    {
        $this->apiContext = $context;
    }
    public function get_cart_detail()
    {
        $subtotal = 0;
        $details = [];
        foreach ($_SESSION['cart'] as $key => $value) {
            $item = new Item();
            $item->setName($value['nombre'])
                ->setCurrency(MONEDA)
                ->setQuantity($value['cantidad'])
                ->setSku("123123")
                ->setPrice($value['precio']);
            $details[] = $item;
            $subtotal += $value['precio'] * $value['cantidad'];
        }

        return (object)[
            "subtotal" => $subtotal,
            "details" => $details
        ];
    }
    public function get_cart_demo()
    {
        $subtotal = 0.01;
        $item = new Item();
        $item->setName("Product sample")
            ->setCurrency(MONEDA)
            ->setQuantity(1)
            ->setSku("P0-00123")
            ->setPrice(0.01);
        $details[] = $item;
        return (object)[
            "subtotal" => $subtotal,
            "details" => $details
        ];
    }
    public function create_payment()
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $items = $this->get_cart_detail();

        $subtotal = $items->subtotal;
        $details = $items->details;
        
        $itemList = new ItemList();
        $itemList->setItems($details);

        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($subtotal);

        $amount = new Amount();
        $amount->setCurrency(MONEDA)
            ->setTotal($subtotal)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment web description")
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(BASE_URL . "gracias?success=true")
            ->setCancelUrl(BASE_URL . "error?success=false");

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        $request = clone $payment;
        
        try {
            $payment->create($this->apiContext);
        } catch (Exception $ex) {            
            return (object)[
                "error" => json_decode($ex),
                "success" => false,
                "msg"   => "error occurred while creating payment."
            ];
        }
        $approvalUrl = $payment->getApprovalLink();
        $payment = json_decode($payment);
        $request = json_decode($request);

        $data = new stdClass();
        $data->success = true;
        $data->msg = "payment created successfull.";
        $data->links = $approvalUrl;
        $data->paymentId = $payment->id;
        $data->invoice_number = $payment->transactions[0]->invoice_number;
        $data->pay_method = $payment->payer->payment_method;

        return $data;        
    }

    public function execute_payment($data)
    {
        $paymentId = $data->paymentId;
        $payment = Payment::get($paymentId, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($data->PayerID);

        $transaction = new Transaction();
        $amount = new Amount();
        $items = $this->get_cart_demo(); // demo data 
        $subtotal = $items->subtotal;
        $details = $items->details;

        $details = new Details();
        $details->setShipping(0)
            ->setTax(0)
            ->setSubtotal($subtotal);

        $amount->setCurrency(MONEDA);
        $amount->setTotal($subtotal);
        $amount->setDetails($details);
        $transaction->setAmount($amount);
        $execution->addTransaction($transaction);

        try {
            $result = $payment->execute($execution, $this->apiContext);
            try {
                $payment = Payment::get($paymentId, $this->apiContext);
            } catch (Exception $ex) {
                $data = [
                    "error" => $ex,
                    "success" => false,
                    "msg" => "Error al realizar el pago"
                ];
                return $data;
            }
        } catch (Exception $ex) {
            $data = [
                "error" => $ex,
                "success" => false,
                "msg" => "Ocurrio error al realizar el pago"
            ];
            return $data;
        }
        return json_decode($payment);
    }
}
