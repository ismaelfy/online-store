jQuery(document).ready(function($) {
    let type_payment = $("input:radio[name='pago']");
    let btn_paypal = $("#btn-paypal");
    let form_payment = $("#form-payment");

    function change_payment(state = false) {
        if (state) {
            btn_paypal.removeClass("btn-warning");
            btn_paypal.addClass("btn-primary");
            btn_paypal.html(`<i class="fas fa-credit-card mr-2"></i> Proceder compra`);
        } else {
            btn_paypal.removeClass("btn-primary");
            btn_paypal.addClass("btn-warning");
            btn_paypal.html(`<i class="fab fa-paypal mr-2"></i> PayPal`);
        }
    }
    change_payment(false);
    let name = "";
    let col_transferencia = $(".col-transferencia");
    $("input:radio[name='pago']").click(function(event) {
        if ($(this).is(":checked")) {
            if ($(this).attr("id") == name) {
                $(this).prop("checked", false);
            }
            name = $(this).attr("id");
        }
        if (!type_payment.is(":checked")) {
            change_payment(false);
            name = "";
        } else {
            change_payment(true);
        }
        if ($("#pago_transferencia").is(":checked")) {
            col_transferencia.css("display", "block");
        } else {
            col_transferencia.css("display", "none");
        }
    });
    type_payment.change(function(event) {        
        if (type_payment.is(":checked")) {
            change_payment(true);
        } else {
            change_payment(false);
        }
    });
    form_payment.submit(function(event) {
        event.preventDefault();
        let type = 3;
        if (type_payment.is(":checked")) {
            type = parseInt($("input:radio[name='pago']:checked").val());
        }
        $.ajax({
            url: "../processing",
            type: "POST",
            dataType: "json",
            data: JSON.stringify({
                action: "created",
                type: type,
            }),
            beforeSend: function() {
                type_payment.attr("disabled", "true");
                btn_paypal.addClass("disabled");
                btn_paypal.html('<i class="fas fa-spinner fa-spin mr-2 "></i> procesando');
            },
        }).done(function(response) {
            const {
                success,
                msg,
                url
            } = response;
            if (success) {
                swal("correcto!", msg, "success");
            }
            if (!success) {
                swal("Advertencia!", msg, "error");
            }
            setTimeout(function() {
                window.location.href = url;
            }, 1500);
        });
    });
});