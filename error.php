<?php
session_start();
require_once 'includes/functions.php';
require 'paypal/config.php';
require_once './paypal/paypal.php';
require_once './app/ventas.php';

// sesion detail cart


if (!isset($_SESSION['payment_detail']) || empty($_SESSION['payment_detail'])) {
    header('Location: ./');
    die();
}
$payment  = $_SESSION['payment_detail'];
$payment->type = 0;
$data = (object)$_GET;


if ($data->success === true) {
    header('Location: ./');
    die();
}

if (!isset($payment->type)) {
    header('Location: ./');
    die();
}

$ventas = new Ventas();

if (isset($_GET['success']) && $_GET['success'] == 'false') {
    $data = (object)$_GET;
    if ($payment->type == 1 || $payment->type == 2) {
        $update = [
            "confirm" => 2,
        ];
        $res = $ventas->update($update, $payment->id_venta);
    }
    if ($payment->type == 3) {
        $errors = 0;
        $errors = ($payment->success) ?  +0 : +1;
        $errors = ($payment->paymentId == '') ? +0 : +1;
        $errors = ($data->token != '') ? +0 : +1;

        if ($errors == 0) {
            $datos = [
                "invoice_number" => $payment->invoice_number,
                "payment_id" => $payment->paymentId,
                "payment_token" => $data->token,
                "confirm" => 2,
            ];
            $ventas->update($datos, $payment->id_venta);
        } else {
            header('Location: ./');
            die();
        }
    }
} else {
    header('Location: ./');
    die();
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Error - Chiry </title>
    <?php display_link(); ?>
</head>

<body>
    <?php
    display_header();
    detail_cart();
    ?>

    <!-- visualizar carrito -->
    <section class="p-0 h-50">
        <div class="container py-5">
            <div class="row pt-3">
                <div class="col-sm-12">
                    <h3> Hubo error o cancelo el pago. Si el error es por otro motivo comunícate con el administrador de la página. </h3>
                </div>
            </div>
        </div>
    </section>

    <?php

    display_footer();
    display_script();
    ?>
    <script>
        jQuery(document).ready(function($) {
            setTimeout(function () {
            window.location.href = '<?=BASE_URL?>';
          }, 1500);
        });
        window.onload = function() {
            var loading = document.getElementById('loading');
            loading.style.visibility = 'hidden';
            loading.style.opacity = '0';
            loading.style.display = 'none';           
        }
        
    </script>

</body>

</html>