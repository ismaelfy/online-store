<?php
session_start();
require_once 'includes/functions.php';

if (!isset($_SESSION['u_inf'])) {
    header('location:./');
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title> chiry - payment </title>
	<?php display_link();?>
</head>
<body>
	<?php
display_header();
detail_cart();
?>
	<section>
		<div class="container-f container">
			<div class="row">
				<div class=".col-sm-12 card detail-payment"></div>
			</div>
		</div>
	</section>
	<?php
display_footer();
display_script();
?>
<script>
	function load_data(page) {
		/*cargar lista de carrito*/
		$.ajax({
				url: './valid',
				method: 'POST',
				data: {
					viewventa: 1,
					idnext: page
				},
			})
			.done(function(data) {
				$('.detail-payment').html(data);
		})
	}
	jQuery(document).ready(function($) {
		load_data(0);

		$("body").delegate(".numpage ", "click", function(event) {
		    event.preventDefault();
		    var valor = this.dataset.page;
		    load_data(valor);
		    /*$.ajax({
		        url: "./validar.php",
		        method: "POST",
		        data: {
		            product: 1,
		            idnext: valor,
		            viewventa: 1
		        },
		    }).done(function(data) {
		        setTimeout(function() {
		            $(".contenedor-item").html(data);
		        }, 300);
		    });*/
		});

		window.onload = function() {
			var loading = document.getElementById('loading');
			loading.style.visibility = 'hidden';
			loading.style.opacity = '0';
			loading.style.display = 'none';
		}
	});
</script>

</body>

</html>