<?php
session_start();
require 'paypal/config.php';
require_once './paypal/paypal.php';
require_once './app/ventas.php';
require_once './app/notificacion.php';
$apiContext = getApiContext();

$Process = new paypal($apiContext);

function save_order($type = 0)
{
    $user         = $_SESSION['u_inf'];
    $Ventas       = new Ventas();
    $Notificacion = new Notificacion();
    $carrito      = $_SESSION['cart'];

    $importe = 0;
    foreach ($carrito as $value) {
        $importe += $value['precio'] * $value['cantidad'];
    }

    $total = $importe + SHIPPING['price'];
    $docs  = [
        "fecha"     => date("Y-m-d H:i:s"),
        "id_cli"    => $user['id'],
        "ndoc"      => $Ventas->Get_Num_Doc(),
        "tipo_pago" => $type,
        "importe"   => $importe,
        "envio"     => SHIPPING['price'],
        "total"     => $total,
        "status"    => 0,
    ];
    $idVenta = $Ventas->save($docs);
    if ($idVenta) {
        foreach ($carrito as $key => $value) {
            $impo    = (intval($value['precio']) * number_format($value['cantidad'], 2, '.', ''));
            $details = [
                "id_venta"    => $idVenta,
                "idprod"      => $value['id'],
                "descripcion" => $value['nombre'],
                "cantidad"    => $value['cantidad'],
                "precio"      => $value['precio'],
                "importe"     => $impo,
            ];
            $Ventas->save_detail($details);
            $Ventas->discount_stock($value['id'], $value['cantidad']);
        }
        $notif = [
            "fecha"       => date("Y-m-d H:i:s"),
            "descripcion" => $docs['ndoc'],
            "estado"      => 0,
        ];
        $Notificacion->save($notif);
        $data = (object) [
            "type"     => $type,
            "success"  => true,
            "id_venta" => $idVenta,
        ];
        return $data;
    } else {

        return false;
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $body = json_decode(file_get_contents('php://input'));
    if (isset($body->action) && $body->action != '') {
        if (!isset($_SESSION['cart']) || empty($_SESSION['cart'])) {
            return sendData(["data" => null, "success" => false, "msg" => "No hay productos en el carrito", "url" => BASE_URL]);
        }
        $carrito = $_SESSION['cart'];
        
        switch ($body->action) {
            case 'created':{
                    if ($body->type == 3) {
                        // payment with paypal
                        $res = $Process->create_payment();
                        if ($res->success) {
                            $result = save_order($body->type);
                            if ($result) {
                                $res->id_venta              = $result->id_venta;
                                $res->type                  = $body->type;
                                $_SESSION['payment_detail'] = $res;
                                unset($_SESSION['cart']);
                                return sendData(["success" => $res->success, "msg" => $res->msg, "url" => $res->links]);
                            }
                            return sendData(["success" => false, "msg" => "ocurrio error al guardar el pedido", "url" => BASE_URL]);
                        }
                        $data                       = (object) ["type" => $body->type, "error" => $res->error, "success" => false];
                        $_SESSION['payment_detail'] = $data;
                        return sendData(["success" => false, "msg" => $res->msg, "url" => BASE_URL . "error?success=false"]);
                    }
                    if ($body->type == 1 || $body->type == 2) {
                        // 1) Transferencia, 2) pago contra entrega
                        $result = save_order($body->type);
                        if ($result) {
                            $_SESSION['payment_detail'] = $result;
                            unset($_SESSION['cart']);
                            return sendData(["success" => true, "url" => BASE_URL . "gracias?success=true", "msg" => "se guarado su pedido"]);
                        }
                        return sendData(["success" => false, "msg" => "ocurrio error al guardar el pedido", "url" => BASE_URL . "error?success=false"]);
                    }
                    return sendData(["success" => false, "msg" => "La operacion ha sido cancelado", "url" => BASE_URL]);
                }
        }

    } else {
        $_SESSION['payment_detail'] = '';
        return sendData(["data" => null, "success" => false, "msg" => 'undefinite action on request']);
    }
}
return sendData(["data" => null, "success" => false, "msg" => 'the request is invalid']);
